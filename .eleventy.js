require("dotenv").config();

const htmlmin = require("html-minifier");
const moment = require("moment");
const pluginRss = require("@11ty/eleventy-plugin-rss");

const passThrough = [
  './src/assets/css',
  './src/assets/fonts',
  './src/assets/img',
  './src/assets/js',
  './src/assets/pdf',
  './src/keybase.txt',
  './src/robots.txt',
  './src/favicon.ico',
  './src/apple-touch-icon.png',
  './src/android-chrome-48x48.png',
  './src/favicon-32x32.png',
  './src/favicon-16x16.png',
  './src/site.webmanifest',
  './src/safari-pinned-tab.svg'
];

module.exports = function(config) {
  passThrough.forEach(path => config.addPassthroughCopy(path));

  config.addPlugin(pluginRss);

  config.addFilter("limit", function(array, limit) {
    return array.slice(0, limit);
  });

  // date filters
  config.addFilter("date", function(date, format) {
    return moment(date).format(format);
  });

  config.addFilter("dateIso", function(date) {
    return moment(date).toISOString();
  });

  config.addTransform("htmlmin", function(content, outputPath) {
    if( outputPath.endsWith(".html") ) {
      let minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      });
      return minified;
    }

    return content;
  });

  return {
    passthroughFileCopy: true,
    dir: {
      input: "src",
      output: "dist"
    }
};
}