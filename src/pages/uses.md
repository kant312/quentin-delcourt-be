---
title: /uses
layout: layouts/main
permalink: /uses/
lead: Here is a collection of software and other stuff I use to get things done 😤
meta:
  description: Here is a collection of software and other stuff I use to get things done 😤
---

_Last update: <time datetime="2020-02-19">19<sup>th</sup> February 2020</time>_

## 💻 Hardware

* Dell XPS 15" - 2014 version ➡️ Good machine, if you don't take into account the very bad battery. I still use this machine as of today and it's more than enough.

## 🐧 Operating system

* [Linux Mint](https://linuxmint.com/) ➡️ Ubuntu-based distro, with an excellent desktop manager named Cinnamon.

## 👨‍💻️ Coding

* [PHPStorm](https://www.jetbrains.com/phpstorm/) ➡️ for work and PHP programming in general
* [VSCode](https://code.visualstudio.com/) ➡️ for personnal projects, editing this website, writing Markdown notes, etc.

## 📑 Version control

* [SmartGit](https://www.syntevo.com/smartgit/) ➡️ Because, like most of you, I have no clue on how to use Git in the CLI.

## ⌨️ CLI

* [Terminator](https://terminator-gtk3.readthedocs.io/en/latest/) ➡️ I don't need tmux if I have Terminator. It makes my life easier as I can easily split a terminal window horizontally or vertically.
* [ohmyzsh](https://github.com/ohmyzsh/ohmyzsh) ➡️ I ditched `bash` in favour of `zsh` thanks to `ohmyzsh`. There's no going back.

## 🔤 Fonts

* In IDEs: [Fira Code](https://github.com/tonsky/FiraCode)
* In terminal: [JetBrains Mono](https://www.jetbrains.com/lp/mono/)

## 🌐 Browsing

* [Firefox](https://www.mozilla.org/en-US/firefox/new/)
  * (and the [Vue.js devtools](https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/) add-on)