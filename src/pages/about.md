---
title: About Quentin
layout: layouts/side-image
image: /assets/img/qde_avatar.jpg
permalink: /about/
lead: I'm a software developer from Brussels, Belgium. I create websites and applications. 
meta:
  description: A little bit about Quentin Delcourt.
---

**Want to get in touch?**

You can reach me <a rel="me" href="mailto:quentin@decourt.be?subject=Hello!">by email</a> or through my [LinkedIn page](https://www.linkedin.com/in/quentindelcourt/).

**More about my skills and experience**

You can <a href="/assets/pdf/Quentin_Delcourt_CV_2025.pdf" download="Quentin_Delcourt_CV_2025.pdf">download my CV</a>.

**Tools of the trade**

This is a [list of the tools I use](/uses).

***

### My developer journey

I have been developing websites since before the dawn of the "Web 2.0".

#### Writing HTML like it's 1998

I started fiddling with web pages in 1998, using Word, Frontpage or Notepad as tools of the trade. Me and the one and only [@yhancik](https://yhnck.xyz/) were using this medium to write an offline webzine exchanged on floppy disks, appropriately called "3 1/2". The amount of readers almost reached the double digits, although this was hard to assess, as they were no traffic logs.

#### School days

In 2001, I started studying computer science at the [Institut Paul Lambin](http://www.vinci.be/fr-be/ipl/) in Brussels. After completing these studies, in November 2004 I was offered a web developer position at Qwentes, a communication company mainly working on Europen Commission contracts.

Qwentes was my second school, where I got to meet two great teachers that would ultimately become friends: [Pierre-Vincent Ledoux](https://www.linkedin.com/in/pvledoux/) and [Jérôme Coupé](https://www.webstoemp.com/).

#### Agencies

I stayed at Qwentes during about 3.5 years, then at [Emakina](https://emakina.com) during about 3.5 years (I seem to be obsessed with this number). 
It is during my stay at Emakina that I met [Julien Moreau](https://www.thylo.be/) with whom we endured many Drupal websites.

#### Freelancing

After that I became a freelance developer and we created [Tcheu!](https://github.com/Tcheu) with Pierre-Vincent Ledoux and Jérôme Coupé. We were later joined by Julien Moreau. We worked for various clients, from agencies to corporations and non-profits.

#### From websites to applications

Nowadays I mainly work on applications, writing PHP as well as JS, but let's not talk about the framework, I don't want to update this page every week.