const fetch = require("node-fetch");
const cmsApiEndpoint = "https://graphql.datocms.com/";

// DatoCMS token
const token = process.env.CMS_TOKEN;

let queryCMS = (query) => {
    return fetch(
        cmsApiEndpoint,
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                query: query 
            })
        }
    ).then((response) => {
        return response.json();
    }).catch(error => console.log(error));
};

let fetchFromCMS = (graphqlQuery, graphqlCountQuery, recordsPerQuery) => {
    return queryCMS(graphqlCountQuery).then((response) => {
        let count = Object.values(response.data)[0].count;
        console.log(`Fetching ${count} records`);
        let pageCount = Math.ceil(count / recordsPerQuery);
        let queries = (new Array(pageCount)).fill(graphqlQuery);
        queries = queries.map((query, index) => {
            const recordsToSkip = index * recordsPerQuery;
            query = query.replace('%RECORDS_PER_QUERY%', recordsPerQuery);
            query = query.replace('%RECORDS_TO_SKIP%', recordsToSkip);
            return queryCMS(query);
        });
        return Promise.all(queries).then(values => {
            return values
                .map(records => Object.values(records.data)[0])
                .reduce((allRecords, records) => {
                    return allRecords.concat(records);
                }, []);
        });
    });
};

module.exports = fetchFromCMS;