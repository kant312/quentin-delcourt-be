module.exports = {
    url: "https://quentin.delcourt.be",
    baseUrl: "/",
    author: "Quentin Delcourt",
    email: "quentin@delcourt.be",
    buildTime: new Date(),
};