// required packages
const fetchFromCMS = require('../../_modules/fetchFromCMS');

const recordsPerQuery = 100;

const countAllLinksQuery = `query MyQuery {
  _allLinksMeta {
    count
  }
}`;

const getAllLinksQuery = `{
  allLinks(
      first: %RECORDS_PER_QUERY%,
      skip: %RECORDS_TO_SKIP%,
    ) {
      id
      name
      url
      websiteOrPerson
      websiteOrPersonUrl
      description(markdown: true)
      date
  }
}`;

// DatoCMS token
const token = process.env.CMS_TOKEN;

// get links
// see https://www.datocms.com/docs/content-delivery-api/first-request#vanilla-js-example
async function getAllLinks() {
  const links = await fetchFromCMS(getAllLinksQuery, countAllLinksQuery, recordsPerQuery);

  // format links objects
  const linksFormatted = links.map((item) => {
    return {
      id: item.id,
      date: item.date,
      dateObj: new Date(item.date),
      name: item.name,
      url: item.url,
      body: item.description,
      via: {
          name: item.websiteOrPerson,
          url: item.websiteOrPersonUrl
      }
    };
  });

  // return formatted links
  return linksFormatted;
}

// export for 11ty
module.exports = getAllLinks;