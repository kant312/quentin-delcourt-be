// required packages
const fetchFromCMS = require('../../_modules/fetchFromCMS');

const recordsPerQuery = 100;

const countAllBlogpostsQuery = `query MyQuery {
  _allBlogpostsMeta {
    count
  }
}`;

const getAllBlogpostsQuery = `query MyQuery {
    allBlogposts(filter: {_status: {eq: published}}, orderBy: publicationDate_DESC) {
        content(markdown: true)
        lead
        publicationDate
        title
        id
        highlight
        slug
      }
  }`;

// DatoCMS token
const token = process.env.CMS_TOKEN;

async function getAllBlogposts() {
  const blogposts = await fetchFromCMS(getAllBlogpostsQuery, countAllBlogpostsQuery, recordsPerQuery);

  const blogpostsFormatted = blogposts.map((item) => {
    return {
      id: item.id,
      slug: item.slug,
      url: `/blog/${item.slug}/index.html`,
      date: item.publicationDate,
      dateObj: new Date(item.publicationDate),
      title: item.title,
      lead: item.lead,
      body: item.content,
      highlight: item.highlight
    };
  });

  return blogpostsFormatted;
}

// export for 11ty
module.exports = getAllBlogposts;
